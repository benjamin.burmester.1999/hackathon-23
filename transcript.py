# https://github.com/Uberi/speech_recognition/blob/master/reference/library-reference.rst
import speech_recognition as sr
import keyboard
import sounddevice
from datetime import datetime as dt


# Teachers could login and state which topic or input this is
# teacher = input()
# course = input() # class is a keyword
# topic/assignment = input()
#

def main():
    # Set the source to the default system microphone 
    global mic
    mic = sr.Microphone()


    # Create recognizer instance
    global r
    r = sr.Recognizer()
    r.dynamic_energy_threshold = True

    button = "x"
    keyboard.add_hotkey(button, transcribing)
    print("Press x to start transcription")
    keyboard.wait()

def transcribing():
    timestamp = dt.now().strftime("%Y-%m-%d_%H-%M-%S")
    filename = f"transcript_{timestamp}.txt"
    # with open(filename, "a") as file:
        # file.write(f"Transcription for {topic}!\n-- By {teacher} for {course}\n\n") 
    with mic as source:
        while True:
            try:
                # Adjust recognizer for ambient noise
                r.adjust_for_ambient_noise(source, duration = 1)
                # Listen to input
                print(f"\nListening... (Press Ctrl+C to exit)")
                audio = r.listen(source, timeout=5)
                # Recognize the audio using pocketsphinx and print the transcription
                recognized = r.recognize_sphinx(audio)
                print(f"Transcription: {recognized}")
                
                with open(filename, "a") as file:
                    timestamp = dt.now().strftime("%H:%M:%S")
                    file.write(f"{timestamp}: {recognized}\n")

            except sr.UnknownValueError:
                print("Could not hear anything.")
            except sr.RequestError as error:
                print(f"Could not request results: {error}")
            except sr.WaitTimeoutError:
                print("No audio input.")
  
if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        print(f"\nRecording stopped.")
