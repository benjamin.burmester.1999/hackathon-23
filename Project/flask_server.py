import os
from flask import Flask, render_template, request, session, redirect, url_for
from flask_socketio import join_room, leave_room, send, SocketIO
import random
from string import ascii_uppercase
import speech_recognition as sr
import keyboard

# app = Flask(__name__) # to make the app run without any
app = Flask(__name__, template_folder='templates', static_folder='static')

app.config["SECRET_KEY"] = "hjhjsdahhds"
socketio = SocketIO(app)

rooms = {}

# Generating room codes
def generate_unique_code(length):
    while True:
        code = ""
        for _ in range(length):
            code += random.choice(ascii_uppercase)
        
        if code not in rooms:
            break
    
    return code

# GET for homepage HTML, POST for joining and creating rooms
@app.route("/", methods=["POST", "GET"])
def home():
    session.clear()
    if request.method == "POST":
        code = request.form.get("code")
        join = request.form.get("join", False)
        create = request.form.get("create", False)

        # Trying to join without a room code
        if join != False and not code:
            return render_template("home.html", error="Please enter a room code.", code=code)
        
        room = code
        # Creating a room
        if create != False:
            room = generate_unique_code(4)
            rooms[room] = {"members": 0, "messages": []}
            # Set the source to the default system microphone 
            global mic
            mic = sr.Microphone()


            # Create recognizer instance
            global r
            r = sr.Recognizer()
            r.dynamic_energy_threshold = True

            button = "x"
            keyboard.add_hotkey(button, transcribing)
            print("Press x to start transcription")

        # Checking if room exists
        elif code not in rooms:
            return render_template("home.html", error="Room does not exist.", code=code)
        
        session["room"] = room
        return redirect(url_for("room"))

    return render_template("home.html")

# Getting the room page
@app.route("/room")
def room():
    room = session.get("room")
    if room is None or room not in rooms:
        return redirect(url_for("home"))

    return render_template("room.html", code=room, messages=rooms[room]["messages"])

# WIP, Part of getting rooms to contain the complete transcription even when joining later
@socketio.on("message")
def message(data):
    room = session.get("room")
    if room not in rooms:
        return 
    
    content = {
        "message": data["data"]
    }
    send(content, to=room)
    rooms[room]["text"].append(content)
    print(f"{data['data']}")

@socketio.on("connect")
def connect(auth):
    room = session.get("room")
    if not room:
        return
    if room not in rooms:
        leave_room(room)
        return
    
    join_room(room)
    rooms[room]["members"] += 1

@socketio.on("disconnect")
def disconnect():
    room = session.get("room")
    leave_room(room)

    if room in rooms:
        rooms[room]["members"] -= 1
        if rooms[room]["members"] <= 0:
            del rooms[room]

def transcribing():
    #timestamp = dt.now().strftime("%Y-%m-%d_%H-%M-%S")
    #filename = f"transcript_{timestamp}.txt"
    #with open(filename, "a") as file:
        #file.write(f"Transcription for {topic}!\n-- By {teacher} for {course}\n\n") 
    with mic as source:
        while True:
            try:
                # Adjust recognizer for ambient noise
                r.adjust_for_ambient_noise(source, duration = 1)
                # Listen to input
                print(f"\nListening... (Press Ctrl+C to exit)")
                audio = r.listen(source, timeout=5)
                # Recognize the audio using pocketsphinx and print the transcription
                recognized = r.recognize_sphinx(audio)
                print(f"Transcription: {recognized}")
                socketio.emit("message", { "data": recognized })
                
                #with open(filename, "a") as file:
                    #timestamp = dt.now().strftime("%H:%M:%S")
                    #file.write(f"{timestamp}: {recognized}\n")

            except sr.UnknownValueError:
                print("Could not hear anything.")
            except sr.RequestError as error:
                print(f"Could not request results: {error}")
            except sr.WaitTimeoutError:
                print("No audio input.")


if __name__ == "__main__":
    socketio.run(app, debug=True)
