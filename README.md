# Hackathon 23

## New Problem: Lessons are impeded by unnecessary questions concerning Aspects already discussed

### Raspberry Pi Setup
+ create bootable SD card with pi imager (Pi OS 64-bit)
+ switch to user root: `sudo su root`
+ install debian packages
    + update: `apt-get update && apt-get upgrade`
    + install portaudio and vim: `sudo apt-get install portaudio19-dev`
+ create virtual environment and activate: `python3 -m venv venv && source venv/bin/activate`
+ install python3 libraries: `pip3 install pyaudio SpeechRecognition pocketsphinx Flask Flask-socketIO keyboard`
+ (sounddevice to suppress messages in terminal)

### 28.09.2023 Program Flowchart Session
https://excalidraw.com/#room=fc298d5fb4cc324d8d94,rz1ZwsEYYNBmBycwNvjAnQ
![image info](PNGs/ProgramFlowchartSketch.png)

### 27.09.2023 Library Brainstorming/MVP/Domain-language Session
https://excalidraw.com/#room=6f537e5db6ed9ae31d3e,0SeEFeIwlg-jhto8JcH8fA
![image info](PNGs/27.09.2023.png)

### 5 Why's

**Problem:** Time is being wasted in lessons by unnecessary questions<br>
**Why are there unnecessary questions?**<br>
Because some students ask questions that could be addressed through active listening.<br>
**Why do students struggle with active listening?**<br>
Because they are frequently distracted during lessons. <br>
**Why are students frequently distracted during lessons?**<br>
Due to external distractions (noise, smartphones, etc.) and internal distractions (e.g. lack of interest, personal issues) that divert their attention, making it challenging to regain focus and the required context of the lesson.<br>
**Why do students find it challenging to regain focus and context once distracted?**<br>
Because missing key information and discussions make it difficult to catch up and understand the lesson.<br>
**Why is missing key information so disruptive in a lesson?**<br>
Because there is no easy way to review past lesson contents or discussions, which could facilitate catching up after distractions.<br>

### Journey Map
![image info](PNGs/Journey_Map.png)

### Personas
![image info](PNGs/Persona_-_Cassandra_Meier.png)

### Ideate: Walt Disney Method
![image info](PNGs/WaltDisneyIdeate.png)

### Problem Statement

Our students struggle to comprehend the lesson contents, when they get distracted or otherwise face difficulties in following along.<br>
Our solution should deliver an overiew of previous inputs, as well as provide the opportunity to refocus on the content within the current lesson.

### Individual Learning goals
Benjamin: learn about how to use, configure and program a raspberry Pi and try to make a suitable application 

Mathieu: get to know unix, improve python knowledge, and automated data processing

Mitja: improve how I personally participate in group work

Yassin: extend python knowledge for raspberry pi usage

---

## ARCHIVE: Old Problem: Group Work Is Inefficient

### Initial Brainstorming
![image info](PNGs/InitialBrainstorming.png)
Excalidraw Session: https://excalidraw.com/#room=bb08ac9fb35ecaed892c,LhqE1E4DVxmBeBFBWU2dyg<br>

**Brainstorming**
-  We begun by narrowing the scope of our ideas by framing them against a certain scenario to get to more specific problems faster
-  After coming up with some problems, we briefly looked at possible solutions, mainly to check if the problem itself could be solved in the scope of our project and as a means to gauge interest in the problem and the direction it could take while implementing a solution.
- we ultimately decided on a derivative on the problem "the organisation in groups takes a lot of unnecessary time"  

### Problem Definition

![image info](PNGs/ProblemDefinition1.png)<br>
Excalidraw Session: https://excalidraw.com/#room=e7ad8308649c9f3c760a,Tn4wR0N5zEXl9MGyX7CTaA

### 5 Why's

**Problem:** Group work in schools wastes time.<br>
**Why does group work in schools seemingly waste time?**<br>
Because teachers may assign group work to cover a packed curriculum, even when the topic at hand might not work well in a group work setting.<br>
**Why do teachers assign group work to cover a packed curriculum?<br>**
Because teachers are focused on working with established teaching material and traditional educational methods. <br>
**Why are teachers focused on established material and education methods?<br>**
Because teachers may not have had the time or training to learn and effectively implement newer, more time-efficient teaching tools and methods into their working 
environment.<br>
**Why have teachers not had the time or training to learn and implement newer teaching tools?<br>**
Because there is a shortage of teachers, increasing the workload of existing teachers and reducing the time they can spend on other endeavors.<br>
**Why is there a shortage of teachers?<br>**
Because of the stressful work environment and high workload, which leads to burnout and high turnover among educators as well as a decreased interest in the profession itself.<br>

### Persona
![image info](PNGs/Persona_-_Andreas_Müller.png)

### Brainstorming
[What is possible with arduino/raspberry pi?](PNGs/Brainstorming_-_Arduino_Raspberry_Pi.png)

