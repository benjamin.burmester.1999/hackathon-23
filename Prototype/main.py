import tkinter as tk
from tkinter import ttk
import speech_recognition as sr

class SpeechToTextApp:
    def __init__(self, root):
        self.root = root
        self.root.title("Speech to Text")
        
        self.style = ttk.Style()
        self.style.configure("TButton", font=("Helvetica", 12))
        
        self.start_button = ttk.Button(root, text="Start", command=self.start_transcription)
        self.stop_button = ttk.Button(root, text="Stop", command=self.stop_transcription)
        self.start_button.pack(pady=10)
        self.stop_button.pack(pady=10)
        
        self.text_display = tk.Text(root, height=10, width=40)
        self.text_display.pack(padx=10, pady=10)
        
        self.recognizer = sr.Recognizer()
        self.microphone = sr.Microphone()
        self.running = False
        self.transcription_output_file = "transcription.txt"
        
    def start_transcription(self):
        self.start_button.config(state="disabled")
        self.stop_button.config(state="active")
        self.running = True
        self.capture_audio()

    def stop_transcription(self):
        self.start_button.config(state="active")
        self.stop_button.config(state="disabled")
        self.running = False

    def capture_audio(self):
        if self.running:
            with self.microphone as source:
                audio_data = self.recognizer.listen(source)
                text = self.recognizer.recognize_google(audio_data)
                self.text_display.insert(tk.END, text + "\n")
                with open(self.transcription_output_file, "a") as file:
                    file.write(text + "\n")
            self.root.after(100, self.capture_audio)

if __name__ == "__main__":
    root = tk.Tk()
    app = SpeechToTextApp(root)
    root.mainloop()
